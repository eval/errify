# errify

ClojureScript project that helps with the monad-pipeline pattern

## what

Clojure has some powerful threading macros that let you create 'pipelines'. From Clojure's [Threading Macros Guide](https://clojure.org/guides/threading_macros#thread-first):
```clojure
(-> person
    (assoc :hair-color :gray)
    (update :age inc))
```

Using these for data manipulation improves readability (left to right, top to bottom) and their use is straightforward.

When using threading macros for higher-level processe

When constructing higher-level pipelines in your application you quickly run into limitations.
When constructing higher-level pipelines in your application things become less straightforward:

```clojure
(-> person
    (validate-email)
    (validate-address)
    (send-invite))
```

The example above raises the question: what happens when the email is not valid? Obviously there's not much use in sending invites when any of the validation steps failed. Should `send-invite` contain logic to recognize this? How should the caller of this pipeline know whether things worked out?

What's missing is 1) something to stop the pipeline once an error occurs and 2) have a way/convention to communicate what went wrong.

This is where the monad-pattern is often used. It works as follows: instead of raising errors to control the flow of execution (or add error-data to the person), we'll make the individual operations return a result that's either an error or a result.


To implement this convention for the pipeline above (along with `validate-email`), we could rewrite it as:

```clojure
(defn validate-email [{email :email :as person}]
  (if (re-find some-email-re email)
    [nil person]
    [:invalid-email nil]))

(let [[err result] (validate-email person)
      [err result] (when-not err (validate-address result))
      [err result] (when-not err (send-invite result))]
      [err result])
```
(There are lot's of ways to implement the monad - this example uses a simple tuple: on error the first item is set (e.g `[:invalid-email nil]`), otherwise the second item is set (e.g. `[nil person]`).

As can be seen from the example: the pipeline stops as soon as the an operation returns an error. The final tuple is returned to the caller. This solves the problems identified above.

It comes at a price though: 1) no longer is there a nice pipeline visible, and 2) the operations needed change: e.g. logic was added to `validate-email` to determine *when* it's an error (ie the failing `re-find`), and how it should look like (ie `:invalid-email`).

This repository tries to ease both concerns.

## threading

First of all the `err->` and `err->>` macros will get our pipeline back:

```clojure
(err-> person
       (validate-email)
       (validate-address)
       (send-invite))
```

As long as individual operations return `[nil result]` the pipeline continues and the *result is passed as first argument* to the next operation (just like `->>`, `err->>` will pass the result as last argument). As soon as an operation returns something like `[err nil]` the pipeline 'shortcircuits' and `[err nil]` is returned.

Now for the second part: to be included in the pipeline an operation should follow the 'tuple-convention' as was needed in `validate-email`.
#Having an ad-hoc operation is therefor somewhat harder than in the case our simple `->` and `->>` cousins:
Let's say we want to include an ad-hoc function:

```clojure
;; enrich the person before sending invites
(err-> person
       (validate-email)
       (#(vector nil (assoc % :secondary-mail "..."))
       (send-invite)))
```
Ugh, and that's just the happy flow (the first item is always nil).

Let's see what we can do to improve ad-hoc operations:
```clojure
;; enrich the person before sending invites
(err-> person
       (validate-email)
       (err/wrap> #(assoc % :secondary-mail "..."))
       (send-invite)))
```

`err/wrap>` lets us include a straightforward (non-monad-aware) function to be part of the pipeline! (NOTE: `wrap>` should be used in `err->`, whereas `wrap>>` in `err->>`).

It's also possible that `send-invite` is not even under our control: no way we could have errified it :(

So instead of rewriting the function, let's wrap it:

```clojure
(err-> person
       (validate-email)
       (err/wrap> send-invite)))
```

Easy!  
Now as this only covers the happy-flowputs the result of `send-invite` in a tuple (eg `[nil]`)
How about the error-flow when sending doesn't go as expected:

Let's take care of the error-flow of `send-invite`:
```clojure
;; enrich the person before sending invites
(err-> person
       (validate-email)
       (err/wrap> send-invite {:err-when false? :err :failed-sending})))
```

What this does is making sure that `[:failed-sending nil]` is returned when `(false? (send-invite result-from-email-validate))` holds.

`:err-when` can be as complex as we want but for simple predicates there exist function shortcuts:
```clojure
(err-> person
  (err/when-false?> send-invite {:err :failed-sending}))
```

Very nice!

In order to keep the pipeline 'flowing' not only should individual operations adhere to a convention but also yield a result that makes sense from the pipeline point of view: when all goes well `send-invite` just returns `true`. As this is the last step of our pipeline, we might want to return `[nil person]` to callers.

While we do care about the ultimate happy-flow return value of our pipeline to callers (eg `[nil person]` might be nice), the `send-invite` might have different plans: it returns just `true` or `false`.  
As we added the case for `false` yet - we can also treat 
`errify` let's us skip the return value for the happy-flow:

```clojure
(err-> person
        ...
       (err/when-false?+> send-invite {:err :failed-sending})))
```
wrap-**p**lus-variants will ignore the (non-error) result of the wrapped function and instead **p**ass on the result that was given. Typically you'll use this for functions that are just there for side-effects.

## summary

We constructed 

 why the need to change the operations to contain error-logic? How is clear from looking at the pipeline


Couple things that stand out: first, our nicely


Their use for data manipulation improves the readability and 
When using these for data manipulation as in the example above, they are quite straightforward
When using these pipelines


## how

When treating errors as values it works easy:

```clojure
(err-> person
       (validate-email)
       (validate-address)
       (send-invite))
```